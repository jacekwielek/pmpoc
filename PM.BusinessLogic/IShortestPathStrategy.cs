﻿using PM.DAL;
using System.Collections.Generic;

namespace PM.BusinessLogic
{
    public interface IShortestPathStrategy
    {
        List<string> GetShortesPath(List<Node> nodes, string fromId, string toId);
    }
}
