﻿using System;
using System.Collections.Generic;
using System.Linq;
using PM.DAL;

namespace PM.BusinessLogic
{
    /// <summary>
    /// Strategy for finding shortest path. It's recursive full graph search approach.
    /// </summary>
    public class ShortestPathStrategy : IShortestPathStrategy
    {
        private readonly List<List<string>> _historyOfSearch = new List<List<string>>();

        public List<string> GetShortesPath(List<Node> nodes, string fromId, string toId)
        {
            var result = new List<string>();

            // Get start node
            var startNode =
                nodes.Where(n => n.NodeId.Equals(fromId, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

            // Check if searched node is adjanced one we are done
            if (startNode.AdjacentNodes.Any(n => n.NodeId.Equals(toId, StringComparison.CurrentCultureIgnoreCase)))
            {
                result.Add(fromId);
                result.Add(toId);
            }
            else
            {
                PerformFullPathSearch(startNode, nodes, toId, fromId);

                // Search the shortest path from all found paths
                result = _historyOfSearch.OrderBy(h => h.Count).FirstOrDefault();
            }

            return result;
        }

        /// <summary>
        /// Performs recursive graph discovery. This algorithm performs full graph search in recursive way. It finds all paths that lead from given element to requested one.
        /// As a result we have all available paths and we can then search for the shortest one.
        /// </summary>
        /// <param name="currentNode"></param>
        /// <param name="nodes"></param>
        /// <param name="toNodeId"></param>
        /// <param name="history"></param>
        private void PerformFullPathSearch(Node currentNode, List<Node> nodes, string toNodeId, string history)
        {
            currentNode.AdjacentNodes.ToList().ForEach(adjancedNodes =>
            {
                if (history.Contains(adjancedNodes.NodeId))
                    return;

                // Check if we have checked this path earlier
                var samePathFound = false;
                _historyOfSearch.ForEach(h =>
                {
                    if (string.Join("|", h).Equals(history))
                    {
                        samePathFound = true;
                    }
                });

                if (samePathFound)
                {
                    // Do nothing and allow to gracefully exit loop iteration
                }
                else
                {
                    // Get details of promising adjanced node
                    var nextNode = nodes.FirstOrDefault(n => n.NodeId.Equals(adjancedNodes.NodeId));
                    var newHistory = history + "|" + adjancedNodes.NodeId;

                    if (nextNode.NodeId.Equals(toNodeId, StringComparison.InvariantCultureIgnoreCase))
                    {
                        _historyOfSearch.Add(newHistory.Split('|').ToList());

                        return;
                    }

                    // Perform new recursive search using promising node as a starting point
                    PerformFullPathSearch(nextNode, nodes, toNodeId, newHistory);
                }
            }
                );
        }
    }
}