﻿using PM.DAL;
using System.Collections.Generic;

namespace PM.BusinessLogic
{
    public interface IConverter
    {
        List<Node> FromXMLToNodes(string xml);
    }
}
