﻿using PM.DAL;
using System.Collections.Generic;
using System.Xml;

namespace PM.BusinessLogic
{
    public class Converter : IConverter
    {
        public List<Node> FromXMLToNodes(string xml)
        {
            List<Node> result = new List<Node>();

            var doc = new XmlDocument();

            doc.LoadXml(xml);

            var nodes = doc.SelectNodes("//node");

            foreach (XmlNode node in nodes)
            {
                var newNode = new Node();
                newNode.NodeId = node["id"].InnerText;
                newNode.label = node["label"].InnerText;
                newNode.AdjacentNodes = new List<AdjacentNode>();

                var adjacentNodes = node.SelectNodes(".//adjacentNodes/id");

                foreach (XmlNode adjacentNode in adjacentNodes)
                {
                    var newAdjacentNode = new AdjacentNode { NodeId = adjacentNode.InnerText };
                    newNode.AdjacentNodes.Add(newAdjacentNode);
                }

                result.Add(newNode);

            }

            return result;
        }
    }
}
