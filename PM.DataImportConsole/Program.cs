﻿using System;
using System.IO;
using System.Text;

namespace PM.DataImportConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // Every app run imports all nodes from import folder, as specified in app.config
            var dataManagementService = new PMData.PMDataManagement();

            bool result;
            bool resultSpecified;

            var xml = ReadAllFilesFromInputFolderAndGenerateCommonXMLString();

            dataManagementService.ImportNodes(xml, out result, out resultSpecified);

            if (result)
            {
                Console.WriteLine("Import was successful");
            }
            else
            {
                Console.WriteLine("Unfortunately there was some issue with Import...");
            }

            Console.WriteLine("Import was successful");

            Console.ReadKey();
        }

        private static string ReadAllFilesFromInputFolderAndGenerateCommonXMLString()
        {
            var currentDir = Directory.GetCurrentDirectory();

            var xml = new StringBuilder();

            xml.AppendLine(@"<?xml version=""1.0"" encoding=""UTF-8""?>");
            xml.AppendLine("<nodes>");
            foreach (string file in Directory.EnumerateFiles(currentDir + @"\Import", "*.xml"))
            {
                xml.AppendLine(File.ReadAllText(file));
            }
            xml.AppendLine("</nodes>");

            return xml.ToString();
        }
    }
}
