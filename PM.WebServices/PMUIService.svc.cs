﻿using PM.DAL;
using System.Collections.Generic;

namespace PM.WebServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PMUIService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PMUIService.svc or PMUIService.svc.cs at the Solution Explorer and start debugging.
    public class PMUIService : IPMUIService
    {
        private readonly IRepository _repository;

        public PMUIService(IRepository repository)
        {
            _repository = repository;
        }

        // Here mapping to simple object would make a sense - no time for it now :(
        public List<Node> GetAllNodes()
        {
            return _repository.GetAll();
        }
    }
}
