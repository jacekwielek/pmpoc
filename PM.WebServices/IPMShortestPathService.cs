﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace PM.WebServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPMShortestPathService" in both code and config file together.
    [ServiceContract]
    public interface IPMShortestPathService
    {
        [OperationContract]
        List<string> GetShortestPath(string fromNodeId, string toNodeId);
    }
}
