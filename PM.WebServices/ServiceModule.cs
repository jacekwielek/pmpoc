﻿using Ninject.Modules;
using PM.BusinessLogic;
using PM.DAL;

namespace WCFServices
{
    public class ServiceModule : NinjectModule
    {
        /// <summary>
        /// Loads the module into the kernel.
        /// </summary>
        public override void Load()
        {
            this.Bind<IRepository>().To<NodeRepository>();
            this.Bind<IShortestPathStrategy>().To<ShortestPathStrategy>();
            this.Bind<IConverter>().To<Converter>();
            //this.Bind<ILog>().ToMethod(ctx => LogManager.GetLogger(ctx.Request.Target.Member.DeclaringType));
        }
    }
}