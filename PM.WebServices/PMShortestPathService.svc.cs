﻿using PM.BusinessLogic;
using PM.DAL;
using System.Collections.Generic;

namespace PM.WebServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PMShortestPathService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PMShortestPathService.svc or PMShortestPathService.svc.cs at the Solution Explorer and start debugging.
    public class PMShortestPathService : IPMShortestPathService
    {
        private readonly IShortestPathStrategy _shortestPathStrategy;
        private readonly IRepository _repository;

        public PMShortestPathService(IShortestPathStrategy shortestPathStrategy, IRepository repository)
        {
            _shortestPathStrategy = shortestPathStrategy;
            _repository = repository;
        }

        public List<string> GetShortestPath(string fromNodeId, string toNodeId)
        {
            if (string.IsNullOrEmpty(fromNodeId) || string.IsNullOrEmpty(toNodeId))
                return null;

            var nodes = _repository.GetAll();

            return _shortestPathStrategy.GetShortesPath(nodes, fromNodeId, toNodeId);
        }
    }
}
