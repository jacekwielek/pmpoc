﻿using PM.BusinessLogic;
using PM.DAL;
using System.Collections.Generic;

namespace PM.WebServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PMDataManagement" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PMDataManagement.svc or PMDataManagement.svc.cs at the Solution Explorer and start debugging.
    public class PMDataManagement : IPMDataManagement
    {
        private readonly IRepository _repository;
        private readonly IConverter _converter;

        public PMDataManagement(IRepository repository, IConverter converter)
        {
            _repository = repository;
            _converter = converter;
        }

        public bool ImportNodes(string xmlDoc)
        {
            // Validate
            if (string.IsNullOrEmpty(xmlDoc))
                return false;
            // Convert from string XMLDocument to requested list
            List<Node> nodesToImport = _converter.FromXMLToNodes(xmlDoc);

            _repository.InsertNodes(nodesToImport);

            return true;
        }
    }
}
