﻿using PM.DAL;
using System.Collections.Generic;
using System.ServiceModel;

namespace PM.WebServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPMUIService" in both code and config file together.
    [ServiceContract]
    public interface IPMUIService
    {
        [OperationContract]
        List<Node> GetAllNodes();
    }
}
