﻿using Ninject;
using Ninject.Web.Common;

namespace WCFServices
{
    /// <summary>
    /// The wcf application.
    /// </summary>
    public class Global : NinjectHttpApplication
    {
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        protected override IKernel CreateKernel()
        {
            return new StandardKernel(new ServiceModule());
        }
    }
}