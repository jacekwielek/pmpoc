﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using PM.BusinessLogic;
using PM.DAL;

namespace PM.UnitTests
{
    [TestFixture]
    public class ShortestPathStrategyTest
    {
        private List<Node> _nodes;
        private readonly IShortestPathStrategy _shortestPathStrategy = new ShortestPathStrategy();

        [SetUp]
        public void Setup()
        {
            _nodes = new List<Node>();

            var XXX = new Node()
            {
                NodeId = "XXX",
                label = "NODE XXX",
                AdjacentNodes = new List<AdjacentNode>() {
                    new AdjacentNode() { NodeId = "ZZZ" },
                    new AdjacentNode() { NodeId = "YYY" },
                    new AdjacentNode() { NodeId = "XZ" },
                    new AdjacentNode() { NodeId = "XY" }
                }
            };

            var YYY = new Node()
            {
                NodeId = "YYY",
                label = "NODE YYY",
                AdjacentNodes = new List<AdjacentNode>() {
                    new AdjacentNode() { NodeId = "XXX" },
                    new AdjacentNode() { NodeId = "ZZZ" },
                    new AdjacentNode() { NodeId = "XY" },
                    new AdjacentNode() { NodeId = "ZY" }
                    }
            };

            var ZZZ = new Node()
            {
                NodeId = "ZZZ",
                label = "NODE ZZZ",
                AdjacentNodes = new List<AdjacentNode>() {
                    new AdjacentNode() { NodeId = "XXX" },
                    new AdjacentNode() { NodeId = "YYY" },
                    new AdjacentNode() { NodeId = "XZ" },
                    new AdjacentNode() { NodeId = "ZY" }
                    }
            };

            var XZ = new Node()
            {
                NodeId = "XZ",
                label = "NODE XZ",
                AdjacentNodes = new List<AdjacentNode>() {
                    new AdjacentNode() { NodeId = "XXX" },
                    new AdjacentNode() { NodeId = "ZZZ" }
                    }
            };

            var XY = new Node()
            {
                NodeId = "XY",
                label = "NODE XY",
                AdjacentNodes = new List<AdjacentNode>() {
                    new AdjacentNode() { NodeId = "XXX" },
                    new AdjacentNode() { NodeId = "YYY" }
                    }
            };

            var ZY = new Node()
            {
                NodeId = "ZY",
                label = "NODE ZY",
                AdjacentNodes = new List<AdjacentNode>() {
                    new AdjacentNode() { NodeId = "YYY" },
                    new AdjacentNode() { NodeId = "ZZZ" }
                    }
            };

            // nodes.Add
            _nodes.Add(XXX);
            _nodes.Add(YYY);
            _nodes.Add(ZZZ);
            _nodes.Add(XZ);
            _nodes.Add(XY);
            _nodes.Add(ZY);
        }

        [Test]
        public void WhenFindShortestPath_CalledWithOptimisticScenario_ThenReturnProperOne()
        {
            var result = _shortestPathStrategy.GetShortesPath(_nodes, "XXX", "ZZZ");

            var expected = new List<string>() { "XXX", "ZZZ" };

            CollectionAssert.AreEqual(expected, result);
        }


        [Test]
        public void WhenFindShortestPath_CalledWithPesimisticScenario_ThenReturnProperOne()
        {
            var result = _shortestPathStrategy.GetShortesPath(_nodes, "XXX", "ZY");

            var expected1 = new List<string>() { "XXX", "ZZZ", "ZY" };
            var expected2 = new List<string>() { "XXX", "YYY", "ZY" };

            var pathFound = false;

            if (result.SequenceEqual(expected1) || result.SequenceEqual(expected2))
            {
                pathFound = true;
            }

            Assert.True(pathFound);
        }
    }
}
