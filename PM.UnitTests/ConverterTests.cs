﻿using NUnit.Framework;
using PM.BusinessLogic;

namespace PM.UnitTests
{
    [TestFixture]
    public class ConverterTests
    {
        private const string EXAMPLE_XML = @"<?xml version=""1.0"" encoding=""UTF-8""?>
        <nodes>
            <node>
                <id>XXX</id>
                <label>This is node XXX</label>
                <adjacentNodes>
                    <id>YYY</id>
                    <id>ZZZ</id>
                </adjacentNodes>
            </node>
            <node>
                <id>YYY</id>
                <label>This is node YYY</label>
                <adjacentNodes>
                    <id>XXX</id>
                    <id>ZZZ</id>
                </adjacentNodes>
            </node>
            <node>
                <id>ZZZ</id>
                <label>This is node ZZZ</label>
                <adjacentNodes>
                    <id>XXX</id>
                    <id>YYY</id>
                </adjacentNodes>
            </node>
        </nodes>
        ";

        [Test]
        public void FromXMLToNodes_WhenCalled_ThenGetProperOutput()
        {
            IConverter converter = new Converter();

            var convertedList = converter.FromXMLToNodes(EXAMPLE_XML);

            Assert.IsNotNull(convertedList);
            Assert.IsNotNull(convertedList.Count == 3);
        }
    }
}
