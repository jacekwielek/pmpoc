﻿using System.Collections.Generic;

namespace PM.DAL
{
    public interface IRepository
    {
        Node GetByNodeId(string id);
        void DeleteNode(string id);
        void UpdateNode(Node node);
        Node InsertNode(Node node);
        List<Node> InsertNodes(List<Node> nodeList);
        List<Node> GetAll();
    }
}
