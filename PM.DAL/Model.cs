namespace PM.DAL
{
    using System.Data.Entity;

    public partial class PerformMediaDBContext : DbContext
    {
        public PerformMediaDBContext()
            : base("name=PerformMediaConnectionString")
        {
        }

        public virtual DbSet<AdjacentNode> AdjacentNodes { get; set; }
        public virtual DbSet<Node> Nodes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Node>()
                .HasMany(e => e.AdjacentNodes)
                .WithRequired(e => e.Node)
                .HasForeignKey(e => e.ParentId)
                .WillCascadeOnDelete(false);
        }
    }
}
