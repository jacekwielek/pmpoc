﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Data.Entity;

namespace PM.DAL
{
    public class NodeRepository : IRepository
    {
        public List<Node> GetAll()
        {
            List<Node> nodes;
            using (var context = new PerformMediaDBContext())
            {
                nodes = context.Nodes.Include(n => n.AdjacentNodes).ToList();
            }

            return nodes;
        }

        public void DeleteNode(string id)
        {
            using (var context = new PerformMediaDBContext())
            {
                var nodeToDelete = context.Nodes.Where(n => n.NodeId.Equals(id, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                context.AdjacentNodes.RemoveRange(nodeToDelete.AdjacentNodes);

                context.Nodes.Remove(nodeToDelete);

                context.SaveChanges();
            }
        }

        public Node GetByNodeId(string id)
        {
            Node node;

            using (var context = new PerformMediaDBContext())
            {
                node = context.Nodes.Where(n => n.NodeId.Equals(id, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            }

            return node;
        }

        public Node InsertNode(Node node)
        {
            using (var context = new PerformMediaDBContext())
            {
                context.Nodes.Add(node);

                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    var a = ex.Message;
                }
            }

            return node;
        }

        // This one breaks Repo pattern but to make it faster I decided to do that
        public List<Node> InsertNodes(List<Node> nodeList)
        {
            using (var context = new PerformMediaDBContext())
            {
                nodeList.ForEach(n => context.Nodes.Add(n));

                try
                {
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    var a = ex.Message;
                }
            }

            return nodeList;
        }

        public void UpdateNode(Node node)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required))
            {
                DeleteNode(node.NodeId);
                InsertNode(node);
                scope.Complete();
            }
        }
    }
}
