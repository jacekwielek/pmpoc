﻿using NUnit.Framework;
using PM.DAL;
using System.Collections.Generic;
using System.Linq;

namespace PM.SystemTests
{
    [TestFixture]
    public class DALTests
    {
        private readonly IRepository repo = new NodeRepository();
        private Node node;

        [SetUp]
        public void Setup()
        {
            node = new Node() { label = "test node", NodeId = "XYZ", ParentNodeId = "", AdjacentNodes = new List<AdjacentNode>() { new AdjacentNode() { NodeId = "XYZ_ADJACENT" } } };
        }

        [TearDown]
        public void TearDown()
        {
            if (node != null && node.Id != 0)
            {
                repo.DeleteNode(node.NodeId);
            }

            // We cleanup the whole table. This is only because we have locally copied database to Test folder
            repo.GetAll().ForEach(node => repo.DeleteNode(node.NodeId));
        }

        [Test]
        public void WhenInsertNode_Called_ThenCanGetSavedData()
        {
            repo.InsertNode(node);

            Assert.IsNotNull(node.Id);
        }

        [Test]
        public void WhenIGetByNodeId_Called_ThenGetValidNode()
        {
            repo.InsertNode(node);

            var nodeFromRepo = repo.GetByNodeId(node.NodeId);

            Assert.AreEqual(node.Id, nodeFromRepo.Id);
        }

        [Test]
        public void WhenDeleteNode_Called_ThenGetValidNode()
        {
            repo.InsertNode(node);

            repo.DeleteNode(node.NodeId);

            node = repo.GetByNodeId(node.NodeId);

            Assert.IsNull(node);
        }

        [Test]
        public void WhenUpdateNode_Called_ThenNodePersisted()
        {
            repo.InsertNode(node);
            node = repo.GetByNodeId(node.NodeId);

            node.label = "updated label";

            repo.UpdateNode(node);

            var nodeFromRepoAfterUpdate = repo.GetByNodeId(node.NodeId);

            Assert.AreEqual(nodeFromRepoAfterUpdate.label, "updated label");
        }

        [Test]
        public void WhenInsertNodes_Called_ThenNodesPersisted()
        {
            var nodeList = new List<Node>();
            nodeList.Add(new Node { label = "Node 1", NodeId = "xyz1", ParentNodeId = "", AdjacentNodes = new List<AdjacentNode>() { new AdjacentNode() { NodeId = "xyz1adjacent" } } });
            nodeList.Add(new Node { label = "Node 2", NodeId = "xyz2", ParentNodeId = "", AdjacentNodes = new List<AdjacentNode>() { new AdjacentNode() { NodeId = "xyz2adjacent" } } });

            var nodesAfterSave = repo.InsertNodes(nodeList);
            var wereNodesSaved = !nodesAfterSave.Any(n => n.Id == 0);

            Assert.IsTrue(wereNodesSaved);
        }
    }
}
