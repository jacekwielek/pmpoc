
# PM Proof Of Concept

This is result of my play with some project requirements for PM. It's not completed yet because of lack of time.
### What was done

##### PM.DAL
Persistation layer was created, that is implemented as Repository. For some Repository pattern is antipattern but I've decided to use it in that example PM.DAL Database is SQL Server MDF  file attached to PM.DAL project. Connection string is in app.config/web.config.

##### PM.WebServices
WCF services that could be consumed by clients.

##### PM.DataImportConsole
Simple console application that loads nodes to database. Loading is perfomed in that way that it removes all prevously loaded nodes from database and then loads new once again.

##### PM.BusinessLogic 
So called Business Logic layer. Here we have logic for searching shortest path, converting XML Document to 
our form of nodes.

##### PM.UnitTests 
All unit tests are there.

##### PM.SystemTests

As per name system tests are there. By system tests I mean tests that span system bounded area like communication with database. We have here mainly DAL tests.

## What is missing
Because of lack of time I was not able to implement client side. I've also leak time to polish this project more - because of 4 hrs. time constraint. There should be also mapping between Data Entities to DTO's being send via WCF as now there are deserialization issues. I also think that this solution needs more unit tests that tests core of this application - ShortestPathStrategy. 

During this project I also realised that NOSQL may be better data persistence approach due to not constrained relations between nodes (they may be loaded in random order)

You may also find much better alghoritms on internet related to searching shortest paths on graph e.g. https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm but as stated in requirements I've created my own, recursive algorithm that should work.

## How to use
Entry point to this POC is PM.POC.sln file located in the root. After opening it you should see all projects that are part of this solution. Every external package that is requested should be downloaded during build proces via Nuget.

The best way is to check tests in solution. They are divided into:
* SystemTests - used mainly for persistation tests
* UnitTests - used for testing single units
